#!/bin/bash

# Simple script to "install" the dropbox-tray
# Author: Tomi Lähteenmäki
# Version: 0.1

# Check that we are superuser
if [[ $EUID -ne 0 ]]; then
        echo "This script must be run as root." 1>&2;
        exit 1;
fi

# Check where we are because I can't remember..
DIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))

# Copy program itself to /usr/bin
cp $DIR/dropbox-tray /usr/bin/
if [ $? -ne 0 ]; then { echo "Unable to copy \"dropbox-tray\" to /usr/bin. Aborting."; exit 1; } else { echo "Copied \"dropbox-tray\" to /usr/bin" } fi


# Copy icons to /opt/dropbox-tray
mkdir -p /opt/dropbox-tray
cp -r $DIR/resources/. /opt/dropbox-tray
if [ $? -ne 0 ]; then { echo "There was error when copying icons to /opt/dropbox-tray. Aborting."; exit 1; } else { echo "Copied icons to /opt/dropbox-tray" } fi


# Done !
echo ""
echo "Installation done!"

exit 0
