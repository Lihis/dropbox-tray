#!/bin/bash

# Simple script to "install" the dropbox-tray
# Author: Tomi Lähteenmäki
# Version: 0.2

# We use tar to extract the archive so check that we have it.
type tar >/dev/null 2>&1 || { echo >&2 "I require \"tar\" but it's not installed. Aborting."; exit 1; }
type wget >/dev/null 2>&1 || { echo >&2 "I require \"wget\" but it's not installed. Aborting."; exit 1; }

# Check that we are superuser
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root." 1>&2;
	exit 1;
fi

# Get latest version number
echo "Getting latest version number from server.";
wget http://lihis.net/repo/versions -O /tmp/lihis_versions >/dev/null 2>&1
if [ $? -ne 0 ]; then { echo "Unable to check version number of the latest \"dropbox-tray\" version. wget gave error code: $?"; echo "Are you connected to internet?"; exit 1; } fi
VERSION=`cat /tmp/lihis_versions | grep "dropbox-tray =" | sed 's/.* = "\(.*\)" ./\1/'`

# Download the latest version
# Show percentage to user when downloading..
ARCHIVE=dropbox-tray_$VERSION.tar.gz
echo -n "Downloading $ARCHIVE: "; echo -n "    "
wget http://lihis.net/repo/dropbox-tray/"$ARCHIVE" -O /tmp/"$ARCHIVE" 2>&1 | grep --line-buffered "%" | sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}'; echo -ne "\n";
if [ ${PIPESTATUS[0]} -ne 0 ]; then
	echo -e "\nUnable to download \"dropbox-tray\" from server.";
	echo "Are you connected to internet? If yes and you suspect there is problem with server or this script report it to \"contact@lihis.net\"";
	exit 1;
fi

# Extract arhive bit by bit..
# First extract the program itself to /usr/bin
tar -zxf /tmp/"$ARCHIVE" --overwrite --strip-components=1 -C /usr/bin/ dropbox-tray/dropbox-tray
if [ $? -ne 0 ]; then
	echo "Error while copying \"dropbox-tray\" to /usr/bin.";
	echo "tar gave error code: $?";
	exit 1;
else
	echo "Copied \"dropbox-tray\" to /usr/bin";
fi

# Extract the icons to /opt/dropbox-tray
mkdir -p /opt/dropbox-tray
tar -zxf /tmp/"$ARCHIVE" --overwrite --strip-components=2 -C /opt/dropbox-tray/ dropbox-tray/resources
if [ $? -ne 0 ]; then
	echo "Error while copying icons to /opt/dropbox-tray.";
	echo "tar gave error code: $?"; 
	exit 1;
else
	echo "Icons copied to /opt/dropbox-tray";
fi

# Done!
echo -e "\nInstallation done!";

exit 0
