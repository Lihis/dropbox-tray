CC              = gcc
SRC_PATH	= src/
SRCEXT		= c
SOURCES         = $(shell find $(SRC_PATH) -type f -name *.$(SRCH_PATH))
BUILD_PATH      = build/
LIBS		= build/libs/
TARGET          = dropbox-tray

all: dropbox-tray

release: dropbox-tray
	$(eval TARGET_VERSION = `cat src/main.c | grep "const char \*version =" | sed 's/.*version = "\(.*\)".*/\1/'`)
	$(eval TMP = /tmp/tray_tmp)
	mkdir -p $(TMP)
	mkdir -p $(TMP)/dropbox-tray
	cp $(BUILD_PATH)$(TARGET) $(TMP)/$(TARGET)
	mkdir -p $(TMP)/$(TARGET)/resources
	cp -r resources/icons/linux $(TMP)/$(TARGET)/resources
	cp -r resources/icons/windows $(TMP)/$(TARGET)/resources
	cp -r resources/local_INSTALL.sh $(TMP)/$(TARGET)/INSTALL.sh
	tar -pzcf release/$(TARGET)_$(TARGET_VERSION).tar.gz -C $(TMP) $(TARGET)
	rm -rf $(TMP)

dropbox-tray: main.o copy_icons.o
	$(CC) $(LIBS)main.o $(LIBS)copy_icons.o -o $(BUILD_PATH)$(TARGET)

main.o: $(SRC_PATH)main.c
	mkdir -p $(BUILD_PATH)
	mkdir -p $(LIBS)
	$(CC) -c $(SRC_PATH)main.c -o $(LIBS)main.o

copy_icons.o: $(SRC_PATH)copy_icons.c
	$(CC) -c $(SRC_PATH)copy_icons.c -o $(LIBS)copy_icons.o

clean:
	rm -rf $(BUILD_PATH)*
	rm -rf release/*
