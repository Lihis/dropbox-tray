#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include "copy_icons.h"


void version()
{
        const char *version = "2.1.5";

        printf("#\n");
        printf("# dropbox-tray - The Dropbox tray icon changer\n");
        printf("# Version %s\n", version);
        printf("#\n");
        printf("# Dropbox icons are property of Dropbox, Inc. I do not own any\n");
        printf("# rights to those icons. I'm only supplying this program\n");
        printf("# which you can change Dropnox tray icons to Windows version\n");
        printf("# icons or revert back to Linux version icons.\n");
        printf("#\n");
        printf("# Program written by Lihis\n");
        printf("# http://lihis.net\n");
        printf("#\n");
}

void check_root()
{
	uid_t user;
	user = getuid();

	if (user != 0) {
		printf("Program must be run as root.\n");
		exit(EXIT_FAILURE);
	}
}

void usage()
{
	printf("Usage: dropbox-tray [SELECTOR]\n");
	printf("For more information run: dropbox-tray --help\n");
	exit(0);
}


void delete_files(const char *dir_path)
{
	DIR *dir = opendir(dir_path);
	struct dirent *ent = NULL;
	struct stat statbuf;
	size_t len;
	char *buf;
	int err;

	if (dir == NULL) {
		printf("Error deleting folder: %s.\n", dir_path);
	} else {
		while ((ent = readdir(dir)) != NULL) {
			if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
				continue;

			/* Malloc variable for fullpath */
			len = strlen(dir_path) + strlen(ent->d_name) + 2;
			if (len > PATH_MAX) {
				printf("Error! Path is too long.\n");
				exit(EXIT_FAILURE);
			}
			buf = malloc(len);
			snprintf(buf, len, "%s/%s", dir_path, ent->d_name);

                        /* Check if directory */
                        stat(buf, &statbuf);

			if (S_ISDIR(statbuf.st_mode)) {
				delete_files(buf);
				free(buf);
			} else {
				int val;
				val = unlink(buf);
				if (val != 0)
					printf("Error deleting file: %s\n", buf);
				free(buf);
			}
		}
		err = rmdir(dir_path);
		if (err != 0)
			printf("Error deleting directory %s\n", dir_path);
	}
}

int choice()
{
	char yesno[3];
	int error;

	printf("This will remove \"dropbox-tray\" from your system!\n");
	printf("Are you sure? [yes/no]: ");

	scanf("%s", yesno);

	if (strcmp(yesno, "Yes") == 0 || strcmp(yesno, "yes") == 0) {
		printf("Answered: YES. Deleting program..\n\n");
		delete_files("/opt/dropbox-tray");
		printf("Files deleted. If there were errors you should delete those files by hand.\n");
		printf("Also make sure the program got removed successfully with command:\n");
		printf("  $ which dropbox-tray\n");
		printf("If output says \"which: no dropbox-tray in...\" all got removed then. Thanks!\n");
		return 256;
        } else if (strcmp(yesno, "no") == 0 || strcmp(yesno, "No") == 0) {
		printf("You said NO. Program not deleted.\n");
		return 0;
	} else {
		printf("Invalid option!\n");
		return 0;
	}
	return 0;
}

int main(int argc, char* argv[]) {

	if (argc >= 3) {
		printf("Too many arguments.");
		exit (1);
	} else if (argc > 1) {
		if (strcmp(argv[1],"--install") == 0 || strcmp(argv[1], "-I") == 0) {
			check_root();
			copy_icons("colored");
		} else if (strcmp(argv[1], "--restore") == 0 || strcmp(argv[1], "-R") == 0) {
			check_root();
			copy_icons("gray");
		} else if (strcmp(argv[1], "--version") == 0 || strcmp(argv[1],"-V") == 0) {
                        version();
		} else if (strcmp(argv[1], "--remove-program") == 0) {
			check_root();
			int decisision = choice();
			if (decisision == 256)
				return unlink("/usr/bin/dropbox-tray");
                } else if (strcmp(argv[1],"--help") == 0 || strcmp(argv[1],"-H") == 0) {
                        printf("Usage: dropbox-tray [SELECTOR]\n");
                        printf("   eg. dropbox-tray --restore\n");
                        printf("\n");
                        printf(" -I, --install             Install coloured tray icons\n");
                        printf(" -R, --restore             Restore original tray icons\n");
			printf("     --remove-program      Remove this program and it's files\n");
                        printf(" -H, --help                Print this help and exit\n");
                        printf(" -V, --version             Print version and exit\n");
                        printf("\n");
		} else {
			usage();
		}
	} else {
		usage();
	}

	return 0;

}
