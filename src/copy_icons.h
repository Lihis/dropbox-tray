#ifndef COPY_ICONS_H
#define COPY_ICONS_H

char *combine_path(const char *source, const char *destination, const char icon_num);
void copy_icon(const char *source, const char *destination, int i);
void copy_icons(const char *color);

#endif // COPY_ICONS_H
