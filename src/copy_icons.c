#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

char *combine_path(const char *path, int icon_num)
{
	char* icon[6] = {"dropboxstatus-busy.svg", "dropboxstatus-busy2.svg", "dropboxstatus-idle.svg", "dropboxstatus-logo.svg", "dropboxstatus-x.svg", "dropboxstatus-blank.svg"};
	char *full_path;
	size_t size;

	size = snprintf(NULL, 0, "%s%s", path, icon[icon_num]);
	full_path = (char *)malloc(size + 1);
	snprintf(full_path, size+1, "%s%s", path, icon[icon_num]);

	return full_path;

}

void copy_icon(const char *source, const char *destination, int i)
{
	FILE *copy_source, *copy_destination;
	char ch;

	copy_source = fopen(source, "r");
	if (copy_source == NULL) {
		printf("\r Error copying icon %i/6", i);
		fflush(stdout);
		printf("\nIcon: \"%s\" missing!\n", source);
		exit(EXIT_FAILURE);
	}

	copy_destination = fopen(destination, "w");
	if (copy_destination == NULL) {
		printf("\r ERROR: Unable to copy icon %i/6\n", i + 1);
		fflush(stdout);
		exit(EXIT_FAILURE);
	}

	while (1) {
		ch = fgetc(copy_source);

		if (ch == EOF)
			break;
		else
			putc(ch, copy_destination);
	}

	fclose(copy_source);
	fclose(copy_destination);
}

void copy_icons(const char *color)
{
	const char *destination;
	const char *source;
	char *copy_source;
	char *copy_destination;
	int error;
	int i;

	destination = "/usr/share/icons/ubuntu-mono-dark/apps/22/";
	i = 0;

	if (strcmp(color, "gray") == 0) {
		source = "/opt/dropbox-tray/linux/";
		printf("Starting to copy default Linux style icons.\n");
	} else {
		source = "/opt/dropbox-tray/windows/";
		printf("Starting to copy coloured Windows style icons.\n");
	}

	while (i <= 5) {
		copy_source = combine_path(source, i);
		copy_destination = combine_path(destination, i);

		copy_icon(copy_source, copy_destination, i);

		if (i == 0) {
			printf(" Copied icon %i/6", i + 1);
			fflush(stdout);
		} else if (i >= 5) {
			printf("\r Copied icon %i/6\n", i + 1);
		} else {
			printf("\r Copied icon %i/6", i + 1);
			fflush(stdout);
		}

		i++;
	}
}
